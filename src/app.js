
// imports
import React, { Component } from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';

// styles
import 'normalize.css';
import './styles/globals.css';

// components
import SplashScreen from './components/splash-screen';
import DetailsGray from './components/details-gray';
import DetailsRed from './components/details-red';
import DetailsWhite from './components/details-white';
import BankInterface from './components/bank-interface';
import BigButton from './components/big-button';
import ProdIntro from './components/prod-intro';
import EndingScreen from './components/ending-screen';


// Component: App
export default class App extends Component {

    render () {
        return (
            <Router>
                    <div>
                        { /* splash screen */ }
                        <Route exact path="/" render={props => <SplashScreen campaign="PriorityPay" to="/intro-a" />} />

                        { /* Intros */ }
                        <Route path="/intro-a" render={props => <DetailsGray text={["Instant Cross-Border Payments", "between DBS Accounts", "Across 6 Asian Markets"]} to="/intro-b" />} />
                        <Route path="/intro-b" render={props => <DetailsGray text={["Standard", "Telegraphic Transfers"]} to="/transfer" cta />} />

                        { /* Bank UI */ }
                        <Route path="/transfer" render={props => <BankInterface to="/detail-a" copy="Beneficiary Receives Funds After 1 Week" speed="slow" />} />

                        { /* Results */ }
                        <Route path="/detail-a" render={props => <DetailsRed head={["Long Wait For Payment"]} to="/detail-b" />} />
                        <Route path="/detail-b" render={props => <DetailsRed head={["Sub-Optimal", "Cash Management"]} to="/detail-c" />} />
                        <Route path="/detail-c" render={props => <DetailsRed head={["Impacted Supplier", "Relationship"]} to="/solution" />} />

                        { /* Big button */ }
                        <Route path="/solution" render={props => <BigButton textSmall="Find Out About" textLarge="A Better Solution" to="/prod-intro" />} />

                        { /* Product Introduction */ }
                        <Route path="/prod-intro" render={props => <ProdIntro prodName="PriorityPay" to="/transferBetter" />} />

                        { /* Bank UI 2 */ }
                        <Route path="/transferBetter" render={props => <BankInterface to="/details-d" copy="Beneficiary Receives Payment Instantly" speed="fast" />} />

                        {/* what this means */}
                        <Route path="/details-d" render={props => <DetailsGray text={["What This Means For You"]} to="/details-f" cta />} />

                        {/* Outros */}
                        <Route path="/details-f" render={props => <DetailsRed head={["Smoother business", "operations"]} subHead={{ bottom: ["Better visibility with", "instant credit confirmation"]}} to="/details-g" />} />
                        <Route path="/details-g" render={props => <DetailsRed head={["Better visibility with", "instant credit confirmation"]} subHead={{ top: ["Smoother business", "operations"], bottom: ["Better relationships", "with suppliers"]}} to="/details-h" />} />
                        <Route path="/details-h" render={props => <DetailsRed head={["Better relationships", "with suppliers"]} subHead={{ top: ["Better visibility with", "instant credit confirmation"]}} cta to="/ending-a" />} />

                        { /* Ending */ }
                        <Route path="/ending-a" render={props => <DetailsWhite text={["Instant Cross-Border Payments", "between DBS Accounts", "Across 6 Asian Markets"]}  to="/ending-b" />} />
                        <Route path="/ending-b" render={props => <DetailsWhite text={["PriorityPay"]}  to="/ending-c" />} />

                        <Route path="/ending-c" render={props => <DetailsWhite text={["Dont Wait Any Longer"]}  to="/thend" smallHeader="PriorityPay" />} />
                        <Route path="/thend" render={props => <EndingScreen home="/" replay="/" />} />
                </div>
            </Router>
        )
    }
}
