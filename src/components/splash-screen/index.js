
// imports
import React from 'react';
import { Link } from 'react-router-dom';

import logo from './img/logo.png';
import './styles.css';


// Component: slash screen [stateless]
const SplashScreen = (props) => (
    <Link to={props.to}>
        <div className="container">
            <div className="text-center">
                { /* logo container*/ }
                <div className="logo-container">
                    <img src={logo} alt="DBS Logo" />
                    <h1 className="fc-gradient-gray ff-frBold">{props.campaign}</h1>
                </div>
                <h3 className="ff-arial fc-gray">Touch to Start</h3>

            </div>
        </div>
    </Link>
);

// export
export default SplashScreen;
