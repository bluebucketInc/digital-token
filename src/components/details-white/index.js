
// imports
import React from 'react';
import { withRouter } from 'react-router-dom';
import './styles.css';


// Component: slash screen [stateless]
const DetailsWhite = (props) => {

    // content
    const text = props.text;
    const headers = text.map( text => <h2 className="ff-frBold fc-white" key={text.toString()}>{text}</h2> );

    // redirect if no CTA
    props.cta || setTimeout(() => props.history.push(props.to), props.timeout || 2000);

    // render
    return (
        <div className="container bg-black bg-black-container details-white">
            <div className="text-center valign-center">
                <div className="multiliner">
                    { props.smallHeader &&
                        <h4 className="fc-darkgray ff-frBold">{props.smallHeader}</h4>
                    }
                    {headers}
                </div>
            </div>
        </div>
    )
};

// export
export default withRouter(DetailsWhite);
