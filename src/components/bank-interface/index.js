
// imports
import React, { Component } from 'react';
import ReactModal from 'react-modal';
import TweenMax from 'gsap';
import { Link } from 'react-router-dom';

import logo from './img/logo.png';
import tick from './img/tick.png';
import bigTick from './img/big-tick.png';
import link from './img/link.png';
import dollar from './img/dollar.png';

import './styles.css';

// Component: Bank Interface [stateless]
class BankInterface extends Component {

    constructor() {
        super();

        // slow ease
        this.timeSlow = 4;
        this.easeSlow = "M0,0 C0.192,0.48 0.392,0.576 0.48,0.6 0.55,0.619 0.792,0.688 1,1";
        // fast ease
        this.timeFast = 1.5;
        this.easeFast = "M0,0 C0.192,0.48 0.392,0.576 0.48,0.6 0.55,0.619 0.792,0.688 1,1";

        // layer time
        this.layerTime = 2000;

        // default state
        this.state = {
            loaderModal: false,
            layer1: false,
            layer2: false,
            layer3: false,
            beneficiary: '0'
        };
    }

    // life cycle
    componentWillMount() {
        // default state
        if(this.props.speed === "slow") {
            this.state = {
                loaderModal: false,
                layer1: true,
                layer2: false,
                layer3: false,
                beneficiary: '0'
            };
        }

        // default state
        if(this.props.speed === "fast") {
            this.state = {
                loaderModal: false,
                layer1: false,
                layerTick: true,
                layer2: false,
                layer3: false,
                beneficiary: '0'
            };
        }
    }

    // events - on dropdown chane
    handleBeneficiaryChange = e => {
        this.setState({
            beneficiary: e.target.value
        });
    }

    // events - open modal
    handleOpenModal = e => {
        this.setState({
            loaderModal: true
        });
    }

    // converstion
    doConversion = (e, type) => {

        if (type === "sgd") {
            document.getElementsByName('usd-input')[0].value = (e.target.value / 1.4).toFixed(2);
        } else if (type === 'usd') {
            document.getElementsByName('sgd-input')[0].value = (e.target.value * 1.4).toFixed(2);
        }
    }

    // events - close modal
    handleCloseModal = e => {
        this.setState({
            loaderModal: false
        });
    }

    // trigger animation
    triggerAnimation = () => {

        window.scrollTo(0,0);

        if (this.props.speed === "slow") {
            this.startAnimation(this.timeSlow, this.easeSlow);
        } else if (this.props.speed === "fast") {
            setTimeout(()=>{
                this.setState({
                    layerTick: false,
                    layer2: true,
                });
                setTimeout(()=>this.setState({ layer3: true }), this.layerTime);
            }, this.layerTime);
        }
    }

    // create timeline
    startAnimation = (time, ease) => {

        // vars
        let $item1 = document.getElementById('dollar');
        let $item2 = document.getElementById('loader');

        // animate loader
        TweenMax.to($item1, time, {
            "margin-left": "475px",
            ease: ease
        });

        // animate dollar
        TweenMax.to($item2, time, {
            width: "100%",
            ease: ease,
            // on complete
            onComplete: () => {
                this.setState({
                    layer1: false,
                    layer2: true
                });
                setTimeout(()=>this.setState({ layer3: true }), this.layerTime);
            }
        });
    }

    // render
    render() {

        return (
            <div className='container container-ui bg-white transfer-ui'>

                {/* header */}
                <div className="header">
                    <img src={logo} alt="DBS Logo" />
                </div>

                {/* body */}
                <div className="body">
                    <div className="container container-inner">

                        {/* sec 1 */}
                        <section>

                            <h4 className="sub-header ff-frRoman">Step 1: Transfer from</h4>
                            <div className="grid">
                                <div className="left">
                                    <p className="ff-frRoman">Account</p>
                                </div>
                                <div className="right">
                                    <select disabled>
                                        <option value="1">IBG GTS TRANSSACTION SVS - (BP/FP) 0039008104 (SGD) 50.33</option>
                                    </select>
                                </div>
                            </div>
                            <div className="grid no-margin-bottom">
                                <div className="left">
                                    <p className="ff-frRoman">Amount</p>
                                </div>
                                <div className="right">
                                    <p>Send payee</p>
                                </div>
                            </div>
                            <div className="grid">
                               <div className="left">
                                   <p className="ff-frRoman">&nbsp;</p>
                               </div>
                               <div className="right">
                                   <div className="input-container first-input">
                                       <input className="input-box usd" type="text" value="USD"/>
                                       <input className="input-box" name="usd-input" onChange={e => this.doConversion(e, 'usd')} type="text"/>
                                       <p className="sfont">indicative exchange rate 1 USD = 1.4 SGD </p>
                                       <div className="container-tick">
                                         <img src={tick} width="25"/>
                                         <div className="tick-text">
                                           <p className="sfont">This amount will be sent to your payee</p>
                                         </div>
                                         <div className="link-text">
                                           <p className="sfont">This aproximate amount will be deducted from your account</p>
                                         </div>
                                       </div>

                                       <div className="container-link">
                                           <img src={link} width="45"/>
                                       </div>
                                   </div>
                               </div>
                           </div>
                            <div className="grid no-margin-bottom sec-input">
                               <div className="left">
                                   <p className="ff-frRoman">&nbsp;</p>
                               </div>
                               <div className="right">
                                   <p>Deduct from your account</p>
                               </div>
                           </div>
                            <div className="grid">
                               <div className="left">
                                   <p className="ff-frRoman">&nbsp;</p>
                               </div>
                               <div className="right">
                                   <div className="input-container first-input">
                                       <input className="input-box sgd" type="text" value="SGD"/>
                                       <input className="input-box" name="sgd-input" onChange={e => this.doConversion(e, 'sgd')} type="text"/>
                                   </div>
                               </div>
                           </div>

                        </section>

                        {/* sec 2 */}
                        <section>

                            <h4 className="sub-header ff-frRoman">Step 2: Transfer to</h4>
                            <div className="grid">
                                <div className="left">
                                    <p className="ff-frRoman">Payee</p>
                                </div>
                                <div className="right">
                                    <select onChange={this.handleBeneficiaryChange}  defaultValue={this.state.beneficiary}>
                                        <option value="0" disabled>Select Beneficiary</option>
                                        <option value="1">DBS IDEAL CORP 2 - DHBKHKHHXXX - 788074786</option>
                                    </select>
                                </div>
                            </div>

                            {/* Company Info on select */}
                            <div className="grid">
                                <div className="left"></div>
                                <div className="right">
                                    { this.state.beneficiary === '1' &&
                                        <div className="grid bank-details">
                                            <div className="left">
                                                <p>Company Name</p>
                                            </div>
                                            <div className="right">
                                                {
                                                    this.props.speed === "fast" ?
                                                      <p>Hong Kong</p>
                                                    : <p>Bank X</p>
                                                }
                                                <p className="ff-light fc-gray">SWIFT/BIC: DHBKHKHHXXX</p>
                                                <p className="ff-light fc-gray">Account Number: 010011810</p>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>

                        </section>

                        {/* CTA */}
                        <div className="text-right">
                            <button className="btn btn-red btn-s ff-frBold no-margin-top" onClick={this.handleOpenModal}>Submit</button>
                        </div>

                    </div>
                </div>

                {/*modals*/}
                <ReactModal
                   isOpen={this.state.loaderModal}
                   contentLabel="Transfer Modal"
                   className="modal-inner"
                   overlayClassName="modal-outer animated fadeIn"
                   onAfterOpen={this.triggerAnimation}>

                    {/* the dollar */}
                    { this.state.layer1 &&
                        <div className="loader">
                            <img id="dollar" src={dollar} alt="loader dollar" className="dollar-img" />
                            <div className="loader-outer">
                                <div id="loader" className="loader-inner"></div>
                            </div>
                        </div>
                    }

                    {/* the tick & the dollar */}
                    { this.state.layerTick &&
                        <img src={bigTick} alt="tick image" className="tick-img" />
                    }

                    {/* layers */}
                    { this.state.layer2 &&
                        <div className="text-center">
                            <h3 className="no-margin-bottom fc-white ff-frBold">{this.props.copy}</h3>
                            { this.state.layer3 &&
                                <Link to={this.props.to}>
                                    <button className="btn btn-red btn-m after-load">Next</button>
                                </Link>
                            }
                        </div>
                    }

                </ReactModal>
            </div>
        )
    }
}

// export
export default BankInterface;
