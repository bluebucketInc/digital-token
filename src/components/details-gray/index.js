
// imports
import React from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

import './styles.css';


// Component: slash screen [stateless]
const DetailsGray = (props) => {

    // content
    const text = props.text;
    const headers = text.map( text => <h2 className="ff-frBold fc-gradient-gray" key={text.toString()}>{text}</h2> );

    // redirect if no CTA
    props.cta || setTimeout(() => props.history.push(props.to), props.timeout || 2000);

    // render
    return (
        <div className="text-center container container-m valign-center">
            <div className="multiliner">{headers}</div>
            { props.cta &&
                <Link to={props.to}>
                    <button className="btn btn-red btn-m">Next</button>
                </Link>
            }
        </div>
    )
};

// export
export default withRouter(DetailsGray);
