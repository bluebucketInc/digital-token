
// imports
import React from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

import './styles.css';


// Component: slash screen [stateless]
const DetailsRed = (props) => {

    // possible components
    let headers, subHeadTop, subHeadBottom;

    // content - head
    const text1 = props.head;
    headers = text1.map( text => <h2 className="ff-frBold fc-gradient-red" key={text.toString()}>{text}</h2> );

    // content subHead
    if(props.subHead) {

        // content - subhead - top
        const text2 = props.subHead.top;
        subHeadTop = text2 && text2.map( text => <h4 className="ff-frBold fc-lightgray-a" key={text.toString()}>{text}</h4>);

        // content - subhead - bottom
        const text3 = props.subHead.bottom;
        subHeadBottom = text3 && text3.map( text => <h4 className="ff-frBold fc-lightgray-b" key={text.toString()}>{text}</h4>);
    }

    // redirect if no CTA
    props.cta || setTimeout(() => props.history.push(props.to), props.timeout || 2000);

    // render
    return (
        <div className="text-center container container-m valign-center details-red">

            {/* subHead top */}
            { subHeadTop &&
                <div className="multiliner">{subHeadTop}</div>
            }

            { /* Main */ }
            <div className="multiliner main-head">{headers}</div>

            {/* subhead bottom */}
            { subHeadBottom &&
                <div className="multiliner">{subHeadBottom}</div>
            }

            {/* CTA */}
            { props.cta &&
                <Link to={props.to}>
                    <button className="btn btn-red btn-m">Next</button>
                </Link>
            }
        </div>
    )
};

// export
export default withRouter(DetailsRed);
